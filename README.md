# OpenML dataset: depression

https://www.openml.org/d/43419

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Hi, 
The original Dataset wad published by Frankcc in the following link: Link Kaggle
The dataset is involved into the analysis of depression. The data was consists as a study about the life conditions of people who live in rurales zones. Because all the columns were not explicated in this challenge so We cant understand them. We proceded to delete them or ignoring. Fhe final features or columns were the following:
Content
Surveyid
Villeid
sex
Age
Married
Numberchildren
educationlevel
totalmembers (in the family)
gainedasset
durableasset
saveasset
livingexpenses
otherexpenses
incomingsalary
incomingownfarm
incomingbusiness
incomingnobusiness
incomingagricultural
farmexpenses
laborprimary
lastinginvestment
nolastinginvestmen
depressed: [ Zero: No depressed] or [One: depressed] (Binary for target class)
the main objective is to show statistic analysis and some data mining techniques. 
The dataset has 23 columns or dimensions  and a total of 1432 rows or objects.
Acknowledgements
The original attribution is to Frankcc i
Inspiration

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43419) of an [OpenML dataset](https://www.openml.org/d/43419). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43419/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43419/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43419/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

